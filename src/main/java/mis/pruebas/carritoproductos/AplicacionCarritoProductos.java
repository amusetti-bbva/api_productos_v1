package mis.pruebas.carritoproductos;

import mis.pruebas.carritoproductos.modelo.Producto;
import mis.pruebas.carritoproductos.modelo.Proveedor;
import mis.pruebas.carritoproductos.servicio.ServicioGenerico;
import mis.pruebas.carritoproductos.servicio.impl.ServicioGenericoImpl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.hateoas.config.EnableHypermediaSupport;
import org.springframework.web.filter.ForwardedHeaderFilter;

@SpringBootApplication
@EnableHypermediaSupport(type = EnableHypermediaSupport.HypermediaType.HAL)
@Configuration
public class AplicacionCarritoProductos {

	public static void main(String[] args) {
		SpringApplication.run(AplicacionCarritoProductos.class, args);
	}

	// HTTP request -> [Filtro -> Filtro -> ForwardedHeaderFilter-> Filtro -> Filtro] -> HTTP response
	@Bean
	ForwardedHeaderFilter forwardedHeaderFilter() {
		return new ForwardedHeaderFilter();
	}
	
	@Bean
	ServicioGenerico<Proveedor> servicioProveedor() {
		return new ServicioGenericoImpl<Proveedor>();
	}

	@Bean
	ServicioGenerico<Producto> servicioProducto() {
		return new ServicioGenericoImpl<Producto>();
	}
}
