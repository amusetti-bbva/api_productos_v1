package mis.pruebas.carritoproductos.controlador;

import mis.pruebas.carritoproductos.modelo.Producto;
import mis.pruebas.carritoproductos.modelo.Proveedor;
import mis.pruebas.carritoproductos.servicio.ServicioGenerico;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping("/almacen/v1/productos/{idProducto}/proveedores")
public class ControladorProveedoresProducto {

    @Autowired
    ServicioGenerico<Producto> servicioProducto;

    @Autowired
    ServicioGenerico<Proveedor> servicioProveedor;

    @GetMapping
    public CollectionModel<EntityModel<Proveedor>> obtenerProveedoresProducto(@PathVariable long idProducto) {

        // GET http://localhost:99999/almacen/v1/productos/1/proveedores

        // "self": "http://localhost:9999/almacen/v1/productos/1/proveedores"
        // "producto": "http://localhost:9999/almacen/v1/productos/1"
        final List<Integer> idsProveedores = this.servicioProducto.obtenerPorId(idProducto).getIdsProveedores();
        final List<Proveedor> pp = idsProveedores.stream().map(idProveedor -> this.servicioProveedor.obtenerPorId(idProveedor)).collect(Collectors.toList());

        return CollectionModel.of(pp.stream().map(p -> EntityModel.of(p)
                .add(linkTo(methodOn(ControladorProveedor.class).obtenerProveedorPorId(p.getId())).withSelfRel())
        ).collect(Collectors.toList()))
                .add(linkTo(methodOn(ControladorProducto.class).obtenerProductoPorId(idProducto)).withRel("producto"))
                .add(linkTo(methodOn(this.getClass()).obtenerProveedoresProducto(idProducto)).withSelfRel());
    }

    @GetMapping("/{indiceProveedor}")
    public Proveedor obtenerProveedoresProducto(@PathVariable long idProducto,
                                                @PathVariable int indiceProveedor) {
        // "self" : "http://localhost:9999/almacen/v1/productos/1/proveedores/0"
        // "proveedores" : "http://localhost:9999/almacen/v1/productos/1/proveedores"
        // "producto": "http://localhost:9999/almacen/v1/productos/1"
        final long idProveedor = this.servicioProducto
                .obtenerPorId(idProducto)
                .getIdsProveedores().get(indiceProveedor);
        return this.servicioProveedor.obtenerPorId(idProveedor);
    }

    static class ProveedorProducto {
        public int idProveedor;
    }

    @PostMapping
    public void agregarProveedorProducto(@PathVariable long idProducto,
                                         @RequestBody ProveedorProducto prov) {
        // Location: http://localhost:9999/almacen/v1/productos/1/proveedores/0
        try {
            this.servicioProveedor.obtenerPorId(prov.idProveedor);
            this.servicioProducto.obtenerPorId(idProducto)
                    .getIdsProveedores().add(prov.idProveedor);
        } catch (Exception x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{indiceProveedorProducto}")
    public void borrarProveedorProducto(@PathVariable long idProducto,
                                        @PathVariable int indiceProveedorProducto) {
        try {
            final List<Integer> listaIds = this.servicioProducto
                    .obtenerPorId(idProducto)
                    .getIdsProveedores();
            try {
                listaIds.remove(indiceProveedorProducto);
            } catch(Exception x) {}
        } catch (Exception x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }
}
