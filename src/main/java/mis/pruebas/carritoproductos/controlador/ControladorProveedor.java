package mis.pruebas.carritoproductos.controlador;

import mis.pruebas.carritoproductos.modelo.Proveedor;
import mis.pruebas.carritoproductos.servicio.ServicioGenerico;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin
@RequestMapping("/almacen/v1/proveedores")
public class ControladorProveedor {

    @Autowired
    ServicioGenerico<Proveedor> servicioProveedor; // Inyección de Dependencias

    @GetMapping
    public List<Proveedor> obtenerProveedors() {
        final Map<Long, Proveedor> mp = this.servicioProveedor.obtenerTodos();
        final List<Proveedor> pp = new ArrayList<>();
        for(Long id: mp.keySet()) {
            final Proveedor p = mp.get(id);
            p.setId(id);
            pp.add(p);
        }
        return pp;
    }

    @GetMapping("/{idProveedor}")
    public Proveedor obtenerProveedorPorId(@PathVariable(name = "idProveedor") long id) {
        return this.servicioProveedor.obtenerPorId(id);
    }

    @PostMapping
    public void crearProveedor(@RequestBody Proveedor p) {
        final long id = this.servicioProveedor.agregar(p);
        p.setId(id);
    }

    @PutMapping("/{idProveedor}")
    public void reemplazarProveedorPorId(@PathVariable(name = "idProveedor") long id,
                                        @RequestBody Proveedor p) {
        try {
            this.servicioProveedor.reemplazarPorId(id, p);
        } catch(Exception x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{idProveedor}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void borrarProveedorPorId(@PathVariable(name = "idProveedor") long id) {
        this.servicioProveedor.borrarPorId(id);
    }
}
